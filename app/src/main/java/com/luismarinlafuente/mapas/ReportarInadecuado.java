package com.luismarinlafuente.mapas;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.luismarinlafuente.mapas.base.Comentario;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.luismarinlafuente.mapas.base.Constantes.COMENTARIO;
import static com.luismarinlafuente.mapas.base.Constantes.COMENTAR_INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.REPORT;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;

public class ReportarInadecuado extends AppCompatActivity implements View.OnClickListener{
    Intent intent;
    Comentario comentario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportar_inadecuado);
        intent = getIntent();
        if(intent.getSerializableExtra(COMENTARIO)!=null){
            comentario = (Comentario) intent.getSerializableExtra(COMENTARIO);
            TextView tvLogginReport = (TextView) findViewById(R.id.tvLogginReport);
            TextView tvComentarioRep = (TextView) findViewById(R.id.tvComentariorRep);
            tvLogginReport.setText(comentario.getLoggin());
            tvComentarioRep.setText(comentario.getComentario());
        }
        Button btReport = (Button) findViewById(R.id.btReport);
        btReport.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        EditText etReport = (EditText) findViewById(R.id.etReport);
        switch (view.getId()){
            case R.id.btReport:
                String motivo = etReport.getText().toString();
                Report report = new Report(comentario,motivo,this);
                report.execute();
                intent = new Intent(this, ListadoUbicaciones.class);
                startActivity(intent);
                break;
        }
    }

    private class Report extends AsyncTask<Void,Void,Void>{
        Comentario comentario;
        String motivo;
        Context context;

        public Report(Comentario comentario, String motivo,Context context) {
            this.comentario = comentario;
            this.motivo = motivo;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            try {
                restTemplate.getForObject(URL_SPRING + REPORT + "?id_usuario=" + comentario.getIdUsuario() +
                        "&id_comentario=" + comentario.getId() + "&motivo=" + motivo, Void.class);
               // Toast.makeText(context,"REPORTADO",Toast.LENGTH_LONG).show();
            }catch (Exception e){
                //Toast.makeText(context,"sucedio un error",Toast.LENGTH_LONG).show();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }
}
