package com.luismarinlafuente.mapas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.luismarinlafuente.mapas.base.Constantes.LATITUD;
import static com.luismarinlafuente.mapas.base.Constantes.LONGITUD;
import static com.luismarinlafuente.mapas.base.Constantes._NOTIFICACIONES;

public class GpsCoordinates extends AppCompatActivity implements View.OnClickListener{

    LocationManager locationManager;
    double longitudeBest, latitudeBest;
    double longitudeGPS, latitudeGPS;
    double longitudeNetwork, latitudeNetwork;
    TextView longitudeValueBest, latitudeValueBest;
    TextView longitudeValueGPS, latitudeValueGPS;
    TextView longitudeValueNetwork, latitudeValueNetwork;
    Intent intent;
    Button best, gps, network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps_coordinates);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        longitudeValueBest = (TextView) findViewById(R.id.longitudeValueBest);
        latitudeValueBest = (TextView) findViewById(R.id.latitudeValueBest);
        longitudeValueGPS = (TextView) findViewById(R.id.longitudeValueGPS);
        latitudeValueGPS = (TextView) findViewById(R.id.latitudeValueGPS);
        longitudeValueNetwork = (TextView) findViewById(R.id.longitudeValueNetwork);
        latitudeValueNetwork = (TextView) findViewById(R.id.latitudeValueNetwork);
        bindUI();
    }

    private void bindUI(){
        best = (Button) findViewById(R.id.locationControllerBest);
        gps = (Button) findViewById(R.id.locationControllerGPS);
        network = (Button) findViewById(R.id.locationControllerNetwork);
        best.setOnClickListener(this);
        gps.setOnClickListener(this);
        network.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_gps_location,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itVerEnMapa:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.itAcercaDe:
                intent = new Intent(this,AboutUs.class);
                startActivity(intent);
                return true;
            case R.id.ivVolverA:
                intent = new Intent(this,ListadoUbicaciones.class);
                startActivity(intent);
                return true;
            case R.id.itGps:
                if(latitudeGPS !=0 && longitudeGPS !=0){
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(LATITUD,latitudeGPS);
                    intent.putExtra(LONGITUD,longitudeGPS);
                    startActivity(intent);
                }else {
                    if (_NOTIFICACIONES)
                        Toast.makeText(this,getResources().getString(R.string.no_se_puede_mostrar),Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.itNet:
                if(latitudeNetwork !=0 && longitudeNetwork !=0){
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(LATITUD,latitudeNetwork);
                    intent.putExtra(LONGITUD,longitudeNetwork);
                    startActivity(intent);
                }else {
                    if (_NOTIFICACIONES)
                        Toast.makeText(this,getResources().getString(R.string.no_se_puede_mostrar),Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.itFine:
                if(latitudeBest !=0 && longitudeBest !=0){
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(LATITUD,latitudeBest);
                    intent.putExtra(LONGITUD,longitudeBest);
                    startActivity(intent);
                }else {
                    if (_NOTIFICACIONES)
                        Toast.makeText(this,getResources().getString(R.string.no_se_puede_mostrar),Toast.LENGTH_LONG).show();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

   /* public void toggleGPSUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            locationManager.removeUpdates(locationListenerGPS);
            button.setText(R.string.resume);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
            button.setText(R.string.pause);
        }
    }*/

   /* public void toggleBestUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            locationManager.removeUpdates(locationListenerBest);
            button.setText(R.string.resume);
        } else {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            String provider = locationManager.getBestProvider(criteria, true);
            if (provider != null) {
                locationManager.requestLocationUpdates(provider, 2 * 20 * 1000, 10, locationListenerBest);
                button.setText(R.string.pause);
                Toast.makeText(this, "Best Provider is " + provider, Toast.LENGTH_LONG).show();
            }
        }
    }*/

    /*public void toggleNetworkUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            locationManager.removeUpdates(locationListenerNetwork);
            button.setText(R.string.resume);
        }
        else {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 20 * 1000, 10, locationListenerNetwork);
            Toast.makeText(this, "Network provider started running", Toast.LENGTH_LONG).show();
            button.setText(R.string.pause);
        }
    }*/

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeBest = location.getLongitude();
            latitudeBest = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueBest.setText(longitudeBest + "");
                    latitudeValueBest.setText(latitudeBest + "");
                    if (_NOTIFICACIONES)
                        Toast.makeText(GpsCoordinates.this, "Best Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeNetwork = location.getLongitude();
            latitudeNetwork = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueNetwork.setText(longitudeNetwork + "");
                    latitudeValueNetwork.setText(latitudeNetwork + "");
                    if (_NOTIFICACIONES)
                        Toast.makeText(GpsCoordinates.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {

        }
        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueGPS.setText(longitudeGPS + "");
                    latitudeValueGPS.setText(latitudeGPS + "");
                    if (_NOTIFICACIONES)
                        Toast.makeText(GpsCoordinates.this, "GPS Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.locationControllerBest:
                if (!checkLocation())
                    return;
                if (best.getText().equals(getResources().getString(R.string.pause))) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                    locationManager.removeUpdates(locationListenerBest);
                    best.setText(R.string.resume);
                } else {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    criteria.setAltitudeRequired(false);
                    criteria.setBearingRequired(false);
                    criteria.setCostAllowed(true);
                    criteria.setPowerRequirement(Criteria.POWER_LOW);
                    String provider = locationManager.getBestProvider(criteria, true);
                    if (provider != null) {
                        locationManager.requestLocationUpdates(provider, 2 * 20 * 1000, 10, locationListenerBest);
                        best.setText(R.string.pause);
                        if (_NOTIFICACIONES)
                            Toast.makeText(this, "Best Provider is " + provider, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.locationControllerGPS:
                if (!checkLocation())
                    return;

                if (gps.getText().equals(getResources().getString(R.string.pause))) {
                    locationManager.removeUpdates(locationListenerGPS);
                    gps.setText(R.string.resume);
                } else {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
                    gps.setText(R.string.pause);
                }
                break;
            case R.id.locationControllerNetwork:
                if (!checkLocation())
                    return;
                if (network.getText().equals(getResources().getString(R.string.pause))) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                    locationManager.removeUpdates(locationListenerNetwork);
                    network.setText(R.string.resume);
                }
                else {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, 20 * 1000, 10, locationListenerNetwork);
                    if (_NOTIFICACIONES)
                        Toast.makeText(this, "Network provider started running", Toast.LENGTH_LONG).show();
                            network.setText(R.string.pause);
                }
                break;
        }
    }
}
