package com.luismarinlafuente.mapas;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.luismarinlafuente.mapas.base.Usuario;
import com.luismarinlafuente.mapas.util.Util;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.luismarinlafuente.mapas.base.Constantes.ADD_INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.REPORT_APP;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO_REGISTRADO;

public class Contacto extends AppCompatActivity implements View.OnClickListener{
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);
        EditText etNombreUsuario = (EditText) findViewById(R.id.etNombreUsuario);
        etNombreUsuario.setText(USUARIO_REGISTRADO.getLoggin());
        EditText etEmail = (EditText) findViewById(R.id.etEmailContact);
        etEmail.setText(USUARIO_REGISTRADO.getEmail());
        Button btEnviarReport = (Button) findViewById(R.id.btEnviarReport);
        btEnviarReport.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        EditText etMensaje = (EditText) findViewById(R.id.etIncidenciaInterna);
        String mensaje =  etMensaje.getText().toString();
        switch (view.getId()){
            case R.id.btEnviarReport:
                IssueApp issueApp = new IssueApp(this,mensaje,USUARIO_REGISTRADO,intent);
                issueApp.execute();
                break;
        }
    }
    private class IssueApp extends AsyncTask<Void,Void,Void>{
        Context context;
        String mensaje;
        Usuario usuario;
        Intent intent;

        public IssueApp(Context context, String mensaje, Usuario usuario, Intent intent) {
            this.context = context;
            this.mensaje = mensaje;
            this.usuario = usuario;
            this.intent = intent;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            try {
                restTemplate.getForObject(URL_SPRING + REPORT_APP + "?id_usuario=" + usuario.getId() + "&email=" +usuario.getEmail()+
                        "&mensaje="+mensaje, Void.class);
                intent = new Intent(context,ListadoUbicaciones.class);
                startActivity(intent);
            }catch (Exception e){

            }
            return null;
        }

    }
}
