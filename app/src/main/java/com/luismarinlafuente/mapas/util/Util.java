package com.luismarinlafuente.mapas.util;

import android.content.Context;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

import static com.luismarinlafuente.mapas.base.Constantes.PATRON_FECHA;
import static com.luismarinlafuente.mapas.base.Constantes.PREFERENCES;
import static com.luismarinlafuente.mapas.base.Constantes.SHARED_PREFERENCES;

/**
 * Created by luismarinlafuente on 17/11/17.
 */

public class Util {
    public static Date parseFecha(String fecha){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
        fecha = fecha.replaceAll("T"," ");
        fecha = fecha.replaceAll("Z","");
        try {
            return dateFormat.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static LatLng parseCoordenadas(double latitud, double longitud){
        UTMRef utm = new UTMRef(latitud, longitud, 'N', 30);

        return utm.toLatLng();
    }
    public static String limpiarCampo(String campo){
        //<ul><li>Direcci&oacute;n: CALLE LA SALLE</li><li>Tipo: Cortes de Tráfico</li><li>Inicio: 2017-11-13</li><li>Finalizaci&oacute;n: 2017-12-07</li><li>Motivo: Se va a realizar la pavimentación de las aceras</li></ul>
        campo = campo.replace("<ul>"," ");
        campo = campo.replace("</ul>"," ");
        campo = campo.replace("<li>"," ");
        campo = campo.replace("</li>"," ");
        return campo;
    }
    public static String fechaAtextoInsert(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

        return dateFormat.format(date);
    }
    public static Date textoAFechaParaPintar(String date){
        DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");

        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String textoAFechaParaPintar(Date date){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN);
            return dateFormat.format(date);
    }
    public static String limpiarCaracteresEspeciales(String texto){
        texto = texto.replaceAll("\\*"," ");
        texto = texto.replaceAll("\\r"," ");
        texto = texto.replaceAll("\\n"," ");
        texto = texto.replaceAll("&"," ");
        return texto;
    }
    public static String valueOfDate(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN);
        return simpleDateFormat.format(date);
    }
    public static Date parseFechaSQL(String fecha) {
        DateFormat dateFormat = new SimpleDateFormat(PATRON_FECHA);
        try {
            return dateFormat.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(0);
    }
    public static String formatFechaSQL(Date fecha){
        DateFormat df = new SimpleDateFormat(PATRON_FECHA);
        return df.format(fecha);
    }

}
