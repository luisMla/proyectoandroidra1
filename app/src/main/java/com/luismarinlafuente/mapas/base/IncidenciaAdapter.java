package com.luismarinlafuente.mapas.base;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luismarinlafuente.mapas.R;

import java.util.ArrayList;

import static com.luismarinlafuente.mapas.base.Constantes._SONIDOS;

/**
 * Created by luismarinlafuente on 24/11/17.
 */

public class IncidenciaAdapter extends BaseAdapter {

    private ArrayList<Incidencia> incidencias;
    private Context contexto;
    private LayoutInflater inflater;

    public IncidenciaAdapter(Context contexto, ArrayList<Incidencia> incidencias) {
        this.incidencias = incidencias;
        this.contexto = contexto;
        inflater = LayoutInflater.from(contexto);
    }
    @Override
    public int getCount() {
        return incidencias.size();
    }

    @Override
    public Object getItem(int i) {
        return incidencias.get(i);
    }

    @Override
    public long getItemId(int i) {
        return incidencias.get(i).getId();
    }

    @Override
    public View getView(int posicion, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view == null){
            /**
             * si no existe lo crea entero y se instancia
             *
             */
            view = inflater.inflate(R.layout.incidencia_adapter ,null);
            viewHolder = new ViewHolder();
            /**
             * linea que une los atributos del ViewHolder para que apinten a los elementos de la vista
             */
            viewHolder.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
            viewHolder.tvCalle = (TextView) view.findViewById(R.id.tvCalle);
            viewHolder.tvMotivo = (TextView) view.findViewById(R.id.tvMotivo);
            viewHolder.ivIcono = (ImageView) view.findViewById(R.id.ivIcono);
            viewHolder.linearLayout = view.findViewById(R.id.LLAdapter);
            view.setTag(viewHolder);
        }
        else {
            /**
             * si exixte se recicla
             */
            viewHolder = (ViewHolder) view.getTag();
        }
        /**
         * Incidencia de intercambio
         * propiedad de textView setText() para los campos
         */
        Incidencia incidencia = incidencias.get(posicion);
        viewHolder.tvTitulo.setText(incidencia.getTitulo());
        viewHolder.tvCalle.setText(incidencia.getCalle());
        viewHolder.tvMotivo.setText(incidencia.getMotivo());
        if(posicion % 2 == 0){
            if (_SONIDOS){
                viewHolder.linearLayout.setBackgroundResource(R.color.colorPrimaryDark);

            } else {

                viewHolder.linearLayout.setBackgroundResource(R.color.colorCebra);
            }
        }
        //viewHolder.ivIcono.setImageBitmap();//si el usuario tiene la foto
        switch (incidencia.getTipo()){
            case "Afecciones Importantes":
            case "Cortes de agua":
            case "Cortes de Tráfico":
                viewHolder.ivIcono.setImageResource(android.R.drawable.ic_dialog_info);
                break;
            default:
                viewHolder.ivIcono.setImageResource(android.R.drawable.ic_delete);
                break;
        }
        switch (incidencia.getIcono()){
            case 22:
                viewHolder.ivIcono.setImageResource(android.R.drawable.btn_star_big_on);
                break;
        }

        /**
         * Finalmente devuelve la vista
         */
        return view;

    }

    static class ViewHolder {
        TextView tvTitulo;
        TextView tvCalle;
        TextView tvMotivo;
        ImageView ivIcono;
        LinearLayout linearLayout;
    }
}
