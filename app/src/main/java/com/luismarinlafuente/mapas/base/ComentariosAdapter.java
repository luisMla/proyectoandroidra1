package com.luismarinlafuente.mapas.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luismarinlafuente.mapas.R;

import java.util.ArrayList;

import static com.luismarinlafuente.mapas.base.Constantes._SONIDOS;

/**
 * Created by luismarinlafuente on 26/12/17.
 */

public class ComentariosAdapter extends BaseAdapter{
    private ArrayList<Comentario>comentarios;
    private Context context;
    private LayoutInflater inflater;

    public ComentariosAdapter(ArrayList<Comentario> comentarios, Context context) {
        this.comentarios = comentarios;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return comentarios.size();
    }

    @Override
    public Object getItem(int i) {
        return comentarios.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view == null){
            /**
             * si no existe lo crea entero y se instancia
             *
             */
            view = inflater.inflate(R.layout.comentarios_adapter ,null);
            viewHolder = new ViewHolder();
            /**
             * linea que une los atributos del ViewHolder para que apinten a los elementos de la vista
             */
            viewHolder.tvLoggin = (TextView) view.findViewById(R.id.tvLoggin);
            viewHolder.tvComentario = (TextView) view.findViewById(R.id.tvComentario);
            viewHolder.linearLayout = view.findViewById(R.id.LLAADAPTER);
            view.setTag(viewHolder);
        }
        else {
            /**
             * si exixte se recicla
             */
            viewHolder = (ViewHolder) view.getTag();
        }
        /**
         * Incidencia de intercambio
         * propiedad de textView setText() para los campos
         */
        Comentario comentario = comentarios.get(posicion);
        viewHolder.tvLoggin.setText(comentario.getLoggin());
        viewHolder.tvComentario.setText(comentario.getComentario());
        if(posicion % 2 == 0){
            if (_SONIDOS){
                viewHolder.linearLayout.setBackgroundResource(R.color.colorPrimaryDark);
            } else {
                viewHolder.linearLayout.setBackgroundResource(R.color.colorCebra);
            }
        }
        /**
         * Finalmente devuelve la vista
         */
        return view;
    }
    static class ViewHolder {
        TextView tvLoggin;
        TextView tvComentario;
        LinearLayout linearLayout;

    }
}
