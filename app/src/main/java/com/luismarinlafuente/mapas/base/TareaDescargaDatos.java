package com.luismarinlafuente.mapas.base;

import android.content.Context;
import android.os.AsyncTask;

import com.luismarinlafuente.mapas.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.luismarinlafuente.mapas.base.Constantes.ADD_INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.DELETE_INCIDENCIAS;
import static com.luismarinlafuente.mapas.base.Constantes.GET_INCIDENCIAS_COMENTADAS;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIAS_URL;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIAS_USUARIOS_URL;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;
import static com.luismarinlafuente.mapas.base.Constantes._FAVORITOS;

/**
 * Created by luismarinlafuente on 28/11/17.
 */

public class TareaDescargaDatos extends AsyncTask<Void, Void, Void> {
    private boolean error = false;
    private List<Incidencia> incidencias;
    private Context context;
    private IncidenciaAdapter adapter;
    private boolean refrescar = false;

    public TareaDescargaDatos( ArrayList<Incidencia> incidencias, Context context, IncidenciaAdapter adapter,boolean refrescar) {
        this.incidencias = incidencias;
        this.context = context;
        this.adapter = adapter;
        this.refrescar = refrescar;
    }
//todo arreblar trigger de borrado
    @Override
    protected Void doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        try {
            if(_FAVORITOS){
                BaseDatos db = new BaseDatos(context);
                incidencias.addAll(db.getIncidencias());
            }
            Incidencia[] incidenciasArrayCom = restTemplate.getForObject(URL_SPRING + GET_INCIDENCIAS_COMENTADAS, Incidencia[].class);
            incidencias.addAll(Arrays.asList(incidenciasArrayCom));
            Incidencia[] incidenciasArrayU = restTemplate.getForObject(URL_SPRING + INCIDENCIAS_USUARIOS_URL, Incidencia[].class);
            incidencias.addAll(Arrays.asList(incidenciasArrayU));
            Incidencia[] incidenciasArray = restTemplate.getForObject(URL_SPRING + INCIDENCIAS_URL, Incidencia[].class);
            incidencias.addAll(Arrays.asList(incidenciasArray));
            System.out.println("asdasdasdasdaskjfhasdkfjlahdsfa "+ incidencias.size());
            if(!_FAVORITOS){
                BaseDatos db = new BaseDatos(context);
                incidencias.addAll(db.getIncidencias());
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if (incidencias.size()==0)
            refrescar = true;
        if(refrescar) {
            restTemplate.delete(URL_SPRING+DELETE_INCIDENCIAS);
            String url1 = null;
            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;
            try {
                URL url = new URL(Constantes.URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String linea = null;

                while ((linea = bufferedReader.readLine()) != null) {
                    stringBuilder.append(linea);
                }

                connection.disconnect();
                resultado = stringBuilder.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("result");

                int id;
                String titulo;
                String calle;
                String motivo = null;
                String tramo;
                String observaciones;
                Date inicio;
                Date fin;
                double longitud;
                double latitud;
                String descipcion;
                String tipo;
                Incidencia incidencia = null;
                String[] cooo;
                uk.me.jstott.jcoord.LatLng latLng = null;


                for (int i = 0; i < jsonArray.length(); i++) {
                    id = Integer.parseInt(jsonArray.getJSONObject(i).getString("id"));
                    try {
                        titulo = jsonArray.getJSONObject(i).getString("title");
                    } catch (Exception e) {
                        titulo = "no hay titulo";
                    }
                    try {
                        calle = jsonArray.getJSONObject(i).getString("calle");
                    } catch (Exception e) {
                        calle = "otros";
                    }
                    try {
                        motivo = jsonArray.getJSONObject(i).getString("motivo");
                    } catch (Exception e) {
                        motivo = "No se encuentra motivo";
                    }
                    try {
                        tramo = jsonArray.getJSONObject(i).getString("tramo");
                    } catch (Exception e) {
                        tramo = "no hay tramo";
                    }
                    try {
                        observaciones = jsonArray.getJSONObject(i).getString("observaciones");
                    } catch (Exception e) {
                        observaciones = "no hay observaciones";
                    }
                    try {
                        inicio = Util.parseFecha(jsonArray.getJSONObject(i).getString("inicio"));

                    } catch (Exception e) {
                        inicio = new Date();
                    }
                    try {
                        fin = Util.parseFecha(jsonArray.getJSONObject(i).getString("fin"));

                    } catch (Exception e) {
                        fin = new Date();
                    }
                    try {
                        cooo = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates").replaceAll("\\[|\\]", "").split(",");
                        latitud = Double.parseDouble(cooo[0]);
                        longitud = Double.parseDouble(cooo[1]);
                        latLng = Util.parseCoordenadas(latitud, longitud);
                    } catch (Exception e) {
                        latLng = Util.parseCoordenadas(41.6463433, -0.898877);
                    }
                    try {
                        descipcion = Util.limpiarCampo(jsonArray.getJSONObject(i).getString("description"));
                        descipcion = Util.limpiarCaracteresEspeciales(descipcion);
                    }catch (Exception e){
                        descipcion ="no hay descripcion";
                    }
                    try {
                        tipo = jsonArray.getJSONObject(i).getJSONObject("tipo").getString("title");
                    }catch (Exception e){
                        tipo = "tipo";
                    }
                    incidencia = new Incidencia(id, titulo, calle, motivo, tramo, observaciones, inicio, fin,
                            latLng.getLng(), latLng.getLat(), descipcion, tipo);
                    incidencias.add(incidencia);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RestTemplate restTemplate1 = new RestTemplate();
            restTemplate1.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            for (Incidencia incidencia : incidencias) {
                try {
                    restTemplate1.getForObject(URL_SPRING + ADD_INCIDENCIA + "?titulo=" + incidencia.getTitulo() + "&calle=" + incidencia.getCalle() +
                            "&motivo=" + incidencia.getMotivo() + "&tramo=" + incidencia.getTramo() + "&observaciones=" + incidencia.getObservaciones() +
                            "&inicio=" + Util.fechaAtextoInsert(incidencia.getInicio()) + "&fin=" + Util.fechaAtextoInsert(incidencia.getFin()) + "&longitud=" + incidencia.getLongitud() + "&latitud=" + incidencia.getLatitud() +
                            "&descipcion=" + incidencia.getDescipcion() + "&tipo=" + incidencia.getTipo(), Void.class);

                } catch (Exception e) {

                }
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        incidencias.clear();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        adapter.notifyDataSetChanged();

    }
}
