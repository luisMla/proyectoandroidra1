package com.luismarinlafuente.mapas.base;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by luismarinlafuente on 24/11/17.
 */

public class Incidencia implements Serializable {
    private int id;
    private String titulo;
    private String calle;
    private String motivo;
    private String tramo;
    private String observaciones;
    private Date inicio;
    private Date fin;
    private double longitud;
    private double latitud;
    private String descipcion;
    private String tipo;
    private int icono;
    private int tieneComentarios;

    public Incidencia() {
    }

    public Incidencia(int id, String titulo, String calle, String motivo, String tramo,
                      String observaciones, Date inicio, Date fin,
                      double longitud, double latitud, String descipcion, String tipo) {
        this.id = id;
        this.titulo = titulo;
        this.calle = calle;
        this.motivo = motivo;
        this.tramo = tramo;
        this.observaciones = observaciones;
        this.inicio = inicio;
        this.fin = fin;
        this.longitud = longitud;
        this.latitud = latitud;
        this.descipcion = descipcion;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTramo() {
        return tramo;
    }

    public void setTramo(String tramo) {
        this.tramo = tramo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public int getTieneComentarios() {
        return tieneComentarios;
    }

    public void setTieneComentarios(int tieneComentarios) {
        this.tieneComentarios = tieneComentarios;
    }

    @Override
    public String toString() {
        return "Incidencia{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", calle='" + calle + '\'' +
                ", motivo='" + motivo + '\'' +
                ", tramo='" + tramo + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", inicio=" + inicio +
                ", fin=" + fin +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                ", descipcion='" + descipcion + '\'' +
                ", tipo='" + tipo + '\'' +
                ", icono=" + icono +
                '}';
    }
}
