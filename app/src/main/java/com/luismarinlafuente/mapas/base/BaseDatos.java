package com.luismarinlafuente.mapas.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.luismarinlafuente.mapas.util.Util;

import java.util.ArrayList;
import java.util.Date;

import static com.luismarinlafuente.mapas.base.Constantes.CALLE;
import static com.luismarinlafuente.mapas.base.Constantes.DESCRIPCION;
import static com.luismarinlafuente.mapas.base.Constantes.FIN;
import static com.luismarinlafuente.mapas.base.Constantes.ID;
import static com.luismarinlafuente.mapas.base.Constantes.INICIO;
import static com.luismarinlafuente.mapas.base.Constantes.LATITUD;
import static com.luismarinlafuente.mapas.base.Constantes.LONGITUD;
import static com.luismarinlafuente.mapas.base.Constantes.MOTIVO;
import static com.luismarinlafuente.mapas.base.Constantes.OBSERVACIONES;
import static com.luismarinlafuente.mapas.base.Constantes.TABLA_FAVORITOS;
import static com.luismarinlafuente.mapas.base.Constantes.TIPO;
import static com.luismarinlafuente.mapas.base.Constantes.TITULO;
import static com.luismarinlafuente.mapas.base.Constantes.TRAMO;

/**
 * Created by luismarinlafuente on 22/12/17.
 */

public class BaseDatos extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "favoritos.db";
    private static final int DATABASE_VERSION = 1;
    public BaseDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLA_FAVORITOS+"("+ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                TITULO+" TEXT, "+CALLE+" TEXT,"+MOTIVO+" TEXT,"+ TRAMO +" TEXT,"+OBSERVACIONES+" TEXT,"+INICIO+" TEXT,"+FIN+" TEXT,"+
                LONGITUD+" TEXT,"+LATITUD+" TEXT,"+DESCRIPCION+" TEXT,"+TIPO+" TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLA_FAVORITOS);
        onCreate(db);
    }
    public void insertarIncidencia(Incidencia incidencia){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITULO,incidencia.getTitulo());
        values.put(CALLE,incidencia.getCalle());
        values.put(MOTIVO,incidencia.getMotivo());
        values.put(TRAMO, incidencia.getTramo());
        values.put(OBSERVACIONES,incidencia.getObservaciones());
        values.put(INICIO, Util.fechaAtextoInsert(incidencia.getInicio()));
        values.put(FIN, Util.fechaAtextoInsert(incidencia.getFin()));
        values.put(LONGITUD,String.valueOf(incidencia.getLongitud()));
        values.put(LATITUD,String.valueOf(incidencia.getLatitud()));
        values.put(DESCRIPCION,incidencia.getDescipcion());
        values.put(TIPO,incidencia.getTipo());
        db.insertOrThrow(TABLA_FAVORITOS,null,values);
    }
    public void updateIncidencia(Incidencia incidencia){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITULO,incidencia.getTitulo());
        values.put(CALLE,incidencia.getCalle());
        values.put(MOTIVO,incidencia.getMotivo());
        values.put(TRAMO, incidencia.getTramo());
        values.put(OBSERVACIONES,incidencia.getObservaciones());
        values.put(INICIO, Util.formatFechaSQL(incidencia.getInicio()));
        values.put(FIN, Util.formatFechaSQL(incidencia.getFin()));
        values.put(LONGITUD,String.valueOf(incidencia.getLongitud()));
        values.put(LATITUD,String.valueOf(incidencia.getLatitud()));
        values.put(DESCRIPCION,incidencia.getDescipcion());
        values.put(TIPO,incidencia.getTipo());
        String[] argumentos = new String[]{String.valueOf(incidencia.getId())};
        String where = ID +" = ? " ;
        db.update(TABLA_FAVORITOS, values, where,argumentos);
        db.close();
    }
    public void eliminarIncidencia(long id){
        SQLiteDatabase db = getWritableDatabase();
        String where = ID +" = ? " ;
        String[] argumentos = new String[]{String.valueOf(id)};
        db.delete(TABLA_FAVORITOS ,where,argumentos);
    }
    public ArrayList<Incidencia> getIncidencias()  {
        SQLiteDatabase db = getReadableDatabase();

        ArrayList<Incidencia> incidenciaArrayList = new ArrayList<>();
        final String SELECT[] = {ID,TITULO, CALLE, MOTIVO, TRAMO,OBSERVACIONES,INICIO,FIN,LONGITUD,LATITUD,DESCRIPCION,TIPO};
        Cursor cursor = db.query(TABLA_FAVORITOS,SELECT,null,null,null,null,null);
        Incidencia incidencia = null;
        while (cursor.moveToNext()){
            incidencia = new Incidencia();
            incidencia.setId(cursor.getInt(0));
            incidencia.setTitulo(cursor.getString(1));
            incidencia.setCalle(cursor.getString(2));
            incidencia.setMotivo(cursor.getString(3));
            incidencia.setTramo(cursor.getString(4));
            incidencia.setObservaciones(cursor.getString(5));
            incidencia.setInicio(Util.parseFechaSQL(cursor.getString(6)));
            incidencia.setFin(Util.parseFechaSQL(cursor.getString(7)));
            incidencia.setLongitud(Double.parseDouble(cursor.getString(8)));
            incidencia.setLatitud(Double.parseDouble(cursor.getString(9)));
            incidencia.setDescipcion(cursor.getString(10));
            incidencia.setTipo(cursor.getString(11));
            incidencia.setIcono(22);

            incidenciaArrayList.add(incidencia);
        }
        cursor.close();
        db.close();
        return incidenciaArrayList;
    }
}
