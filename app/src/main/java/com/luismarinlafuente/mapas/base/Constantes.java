package com.luismarinlafuente.mapas.base;

import android.content.SharedPreferences;
import android.graphics.Point;

import java.util.Date;

import javax.xml.parsers.SAXParser;

/**
 * Created by luismarinlafuente on 28/11/17.
 */

public class Constantes {
    public final static String URL= "https://www.zaragoza.es/sede/servicio/via-publica/incidencia.json";
    public final static String INCIDENCIA = "incidencia";
    public final static String USUARIO ="usuario";
    public final static String LISTA_COMPLETA= "listaCompleta";
    public final static String URL_SPRING ="http://10.0.2.2:8080";
    //public final static String URL_SPRING ="http://192.168.1.13:8080";
    public final static String ADD_INCIDENCIA ="/add_incidencia";
    public final static String ADD_INCIDENCIA_USUARIO = "/add_incidencia_usuario";
    public final static String MODIFICAR_INCIDENCIA_URL = "/modificar_incidencia";
    public final static String ELIMINAR_INCIDENCIA_URL = "/borrar_una_incidencia";
    public final static String COMENTAR_INCIDENCIA = "/cometar_incidencia";
    public final static String GET_COMENTARIOS ="/get_comentarios";
    public final static String GET_INCIDENCIAS_COMENTADAS="/incidencias_comentadas";
    public final static String DELETE_INCIDENCIAS = "/borrar";
    public final static String INCIDENCIAS_URL="/incidencias";
    public final static String INCIDENCIAS_USUARIOS_URL ="/incidencias_usuarios";
    public final static String POINT = "point";
    public final static String COMENTARIO ="comentario";
    public final static String REPORT ="/report";

    public final static String GET_USUARIO ="/get_usuario";
    public final static String ADD_USUARIO = "/add_usuario";
    public final static String REPORT_APP="/report_app";

    public static final String TABLA_FAVORITOS = "favoritos";
    public static final String ID = "id";
    public static final String TITULO = "titulo";
    public static final String CALLE = "calle";
    public static final String MOTIVO = "motivo";
    public static final String TRAMO = "tramo";
    public static final String OBSERVACIONES = "observaciones";
    public static final String INICIO = "inicio";
    public static final String FIN = "fin";
    public static final String LONGITUD = "longitud";
    public static final String LATITUD ="latitud";
    public static final String DESCRIPCION = "descripcion";
    public static final String TIPO = "tipo";

    // Constantes para las preferencias
    public static Usuario USUARIO_REGISTRADO;
    public static SharedPreferences SHARED_PREFERENCES;
    public static final String PREFERENCES = "PREFERENCES";

    public static final String SONIDOS = "SONIDOS";
    public static final String FAVORITOS_PRIMERO = "FAVORITOS_PRIMERO";
    public static final String NOTIFICACIONES = "NOTIFICACIONES";

    public static boolean _SONIDOS ;
    public static boolean _NOTIFICACIONES ;
    public static boolean _FAVORITOS ;

    public static final String PATRON_FECHA = "dd/mm/yyyy";
}
