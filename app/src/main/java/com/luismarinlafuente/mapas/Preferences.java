package com.luismarinlafuente.mapas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import static com.luismarinlafuente.mapas.base.Constantes.FAVORITOS_PRIMERO;
import static com.luismarinlafuente.mapas.base.Constantes.LISTA_COMPLETA;
import static com.luismarinlafuente.mapas.base.Constantes.NOTIFICACIONES;
import static com.luismarinlafuente.mapas.base.Constantes.PREFERENCES;
import static com.luismarinlafuente.mapas.base.Constantes.SHARED_PREFERENCES;
import static com.luismarinlafuente.mapas.base.Constantes.SONIDOS;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO_REGISTRADO;
import static com.luismarinlafuente.mapas.base.Constantes._FAVORITOS;
import static com.luismarinlafuente.mapas.base.Constantes._NOTIFICACIONES;
import static com.luismarinlafuente.mapas.base.Constantes._SONIDOS;

public class Preferences extends AppCompatActivity implements View.OnClickListener{
    SharedPreferences.Editor edit;
    Switch fSwitch, sSwitch, nSwitch;
    Button btGuardar;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
    }

    public void bindUI(){

        fSwitch = (Switch) findViewById(R.id.swFavoritos);
        fSwitch.setChecked(_FAVORITOS);
        sSwitch = (Switch) findViewById(R.id.swSonidos);
        sSwitch.setChecked(_SONIDOS);
        nSwitch = (Switch) findViewById(R.id.swNotificacione);
        nSwitch.setChecked(_NOTIFICACIONES);
        btGuardar = (Button) findViewById(R.id.btGuardarPref);
        btGuardar.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        bindUI();
        SHARED_PREFERENCES = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btGuardarPref:
                edit =  SHARED_PREFERENCES.edit();
                edit.putBoolean(USUARIO_REGISTRADO.getLoggin() + FAVORITOS_PRIMERO, fSwitch.isChecked());
                System.out.println(USUARIO_REGISTRADO.getLoggin() + FAVORITOS_PRIMERO + "->" + fSwitch.isChecked());
                edit.putBoolean(USUARIO_REGISTRADO.getLoggin() + NOTIFICACIONES, nSwitch.isChecked());
                edit.putBoolean(USUARIO_REGISTRADO.getLoggin() + SONIDOS, sSwitch.isChecked());
                edit.apply();
                if(_NOTIFICACIONES)
                Toast.makeText(this, getResources().getString(R.string.toast_preferences), Toast.LENGTH_LONG).show();


                break;
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itDesconectar:
                USUARIO_REGISTRADO = null;
                intent = new Intent(this, AccesoUsuarios.class);
                startActivity(intent);
                return true;
            case R.id.itVolver:
                intent = new Intent(this, ListadoUbicaciones.class);
                startActivity(intent);

                return true;
        }
        return false;
    }
}
