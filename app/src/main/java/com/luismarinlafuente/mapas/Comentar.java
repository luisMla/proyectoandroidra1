package com.luismarinlafuente.mapas;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.luismarinlafuente.mapas.base.Comentario;
import com.luismarinlafuente.mapas.base.ComentariosAdapter;
import com.luismarinlafuente.mapas.base.Incidencia;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import static com.luismarinlafuente.mapas.base.Constantes.COMENTARIO;
import static com.luismarinlafuente.mapas.base.Constantes.COMENTAR_INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.GET_COMENTARIOS;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO_REGISTRADO;

public class Comentar extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener{
    private ArrayList<Comentario>comentarios;
    private ComentariosAdapter comentariosAdapter;
    private Incidencia incidencia;
    private Intent intent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentar);
        comentarios = new ArrayList<>();
        intent = getIntent();
        if(intent.getSerializableExtra(INCIDENCIA)!=null)
            incidencia = (Incidencia) intent.getSerializableExtra(INCIDENCIA);

        ListView lvComentarios = (ListView) findViewById(R.id.lvComentarios);
        comentariosAdapter = new ComentariosAdapter(comentarios,this);
        lvComentarios.setAdapter(comentariosAdapter);
        registerForContextMenu(lvComentarios);
        lvComentarios.setOnItemClickListener(this);


        TextView tvTitulo1 = (TextView) findViewById(R.id.tvTitulo1);
        TextView tvDescripcion1 = (TextView) findViewById(R.id.tvDescripcion1);
        tvTitulo1.setText(incidencia.getTitulo());
        tvDescripcion1.setText(incidencia.getTramo());

        Button btEnviarComentario = (Button) findViewById(R.id.btEnviarComentario);
        btEnviarComentario.setOnClickListener(this);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.contex_menu_comentarios,menu);
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int posicion, long l) {
        Comentario comentario = comentarios.get(posicion);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemSeleccionado = info.position;
        Comentario comentario = comentarios.get(itemSeleccionado);
        switch (item.getItemId()) {
            case R.id.itReportarInadecuado:
                intent = new Intent(this,ReportarInadecuado.class);
                intent.putExtra(COMENTARIO,comentario);
                startActivity(intent);
                break;

        }
        return super.onContextItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        EditText etComentario1 = (EditText) findViewById(R.id.etComentario1);
        String comentario = etComentario1.getText().toString();
        switch (view.getId()){
            case R.id.btEnviarComentario:
                TareaDescargarComentarios tareaDescargarComentarios = new TareaDescargarComentarios(incidencia,true,comentario,comentarios);
                tareaDescargarComentarios.execute();
                intent = new Intent(this,ListadoUbicaciones.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(incidencia.getTieneComentarios()==1){
            TareaDescargarComentarios tareaDescargarComentarios = new TareaDescargarComentarios(comentarios,this,comentariosAdapter,incidencia,false);
            tareaDescargarComentarios.execute();
            comentariosAdapter.notifyDataSetChanged();
        }
    }


    private class TareaDescargarComentarios extends AsyncTask<Void,Void,Void>{

        private ArrayList<Comentario>comentarios;
        private Context context;
        private ComentariosAdapter comentariosAdapter;
        private Incidencia incidencia;
        private boolean alta ;
        private String comentario;

        public TareaDescargarComentarios(Incidencia incidencia, boolean alta, String comentario, ArrayList<Comentario>comentarios) {
            this.incidencia = incidencia;
            this.alta = alta;
            this.comentario = comentario;
            this.comentarios = comentarios;
        }

        public TareaDescargarComentarios(ArrayList<Comentario> comentarios, Context context, ComentariosAdapter comentariosAdapter,
                                         Incidencia incidencia, boolean alta) {
            this.comentarios = comentarios;
            this.context = context;
            this.comentariosAdapter = comentariosAdapter;
            this.incidencia = incidencia;
            this.alta = alta;
        }

        @Override
        protected void onPreExecute(){
            if(!alta) {
                comentarios.clear();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            if(!alta){
                try{
                    Comentario[] comentariosArray = restTemplate.getForObject(URL_SPRING + GET_COMENTARIOS+"?id="+incidencia.getId(), Comentario[].class);
                    comentarios.addAll(Arrays.asList(comentariosArray));
                }catch (Exception e){

                }
            }
            if(alta){
                try {
                    restTemplate.getForObject((URL_SPRING + COMENTAR_INCIDENCIA + "?id_usuario=" + USUARIO_REGISTRADO.getId() +
                            "&id_incidencia=" + incidencia.getId() + "&comentario=" + comentario).trim(), Void.class);
                }catch (Exception e){
                }
            }

            return null;
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(!alta){
                comentariosAdapter.notifyDataSetChanged();
            }

        }




    }
}
