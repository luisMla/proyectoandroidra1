package com.luismarinlafuente.mapas;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.luismarinlafuente.mapas.base.BaseDatos;
import com.luismarinlafuente.mapas.base.Incidencia;
import com.luismarinlafuente.mapas.base.IncidenciaAdapter;
import com.luismarinlafuente.mapas.base.TareaDescargaDatos;
import com.luismarinlafuente.mapas.base.Usuario;

import java.util.ArrayList;

import static com.luismarinlafuente.mapas.base.Constantes.FAVORITOS_PRIMERO;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.LISTA_COMPLETA;
import static com.luismarinlafuente.mapas.base.Constantes.NOTIFICACIONES;
import static com.luismarinlafuente.mapas.base.Constantes.PREFERENCES;
import static com.luismarinlafuente.mapas.base.Constantes.SHARED_PREFERENCES;
import static com.luismarinlafuente.mapas.base.Constantes.SONIDOS;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO_REGISTRADO;
import static com.luismarinlafuente.mapas.base.Constantes._FAVORITOS;
import static com.luismarinlafuente.mapas.base.Constantes._NOTIFICACIONES;
import static com.luismarinlafuente.mapas.base.Constantes._SONIDOS;

public class ListadoUbicaciones extends AppCompatActivity implements AdapterView.OnItemClickListener{
    private ArrayList<Incidencia> incidencias;
    private IncidenciaAdapter adapter;
    private Intent intent;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_ubicaciones);
        listView = (ListView) findViewById(R.id.lvListado);
        incidencias = new ArrayList<>();
        adapter = new IncidenciaAdapter(this, incidencias);
        //TareaDescargaDatos tareaDescargaDatos = new TareaDescargaDatos(incidencias,this,adapter,false);
        //tareaDescargaDatos.execute();
        registerForContextMenu(listView);
        listView.setOnItemClickListener(this);

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        intent = getIntent();
        if(USUARIO_REGISTRADO!=null){
            onLoadPreferences();
        }
        else {
            Toast.makeText(this, R.string.error_login,Toast.LENGTH_LONG).show();
            intent = new Intent(this, AccesoUsuarios.class);
            startActivity(intent);
        }


    }

    public void onLoadPreferences() {
        SHARED_PREFERENCES = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        _SONIDOS = SHARED_PREFERENCES.getBoolean(USUARIO_REGISTRADO.getLoggin() + SONIDOS, false );
        _NOTIFICACIONES = SHARED_PREFERENCES.getBoolean(USUARIO_REGISTRADO.getLoggin() + NOTIFICACIONES, false );
        _FAVORITOS = SHARED_PREFERENCES.getBoolean(USUARIO_REGISTRADO.getLoggin() + FAVORITOS_PRIMERO, false );
        System.out.println("SONIDOS NOTIFICACIONES FAVORITOS ----> "+ _SONIDOS + " " + _NOTIFICACIONES+ " " + _FAVORITOS);
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        TareaDescargaDatos tareaDescargaDatos = new TareaDescargaDatos(incidencias, this, adapter, false);
        tareaDescargaDatos.execute();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int posicion, long l) {

        Incidencia incidencia = incidencias.get(posicion);
        Intent intent = new Intent(this, MostrarIncidencia.class);
        intent.putExtra(INCIDENCIA,incidencia);
        startActivity(intent);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu,menu);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miVertodos:
                intent = new Intent(this,MainActivity.class);
                intent.putExtra(LISTA_COMPLETA,incidencias);
                startActivity(intent);
                return true;

            case R.id.miVerMapa:
                intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                return true;

            case  R.id.menuActualizar:
                TareaDescargaDatos tareaDescargaDatos = new TareaDescargaDatos(incidencias,this,adapter,true);
                tareaDescargaDatos.execute();
                adapter.notifyDataSetChanged();
                if (_NOTIFICACIONES)
                    Toast.makeText(this,getResources().getString(R.string.actualizado),Toast.LENGTH_LONG).show();
                return true;
            case R.id.itContacto:
                intent = new Intent(this,Contacto.class);
                startActivity(intent);
                return true;
            case R.id.itDesconectar:
                intent = new Intent(this,AccesoUsuarios.class);
                USUARIO_REGISTRADO = null;
                startActivity(intent);
                return true;
            case R.id.itAcercaDe:
                intent = new Intent(this,AboutUs.class);
                startActivity(intent);
                return true;
            case R.id.itGps:
                intent = new Intent(this,GpsCoordinates.class);
                startActivity(intent);
                return true;
            case R.id.itPreferences:
                intent = new Intent(this, Preferences.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemSeleccionado = info.position;
        Incidencia incidencia;
        switch (item.getItemId()) {
            case R.id.modificar:
                intent = new Intent(this,AltaIncidencia.class);
                incidencia = incidencias.get(itemSeleccionado);
                intent.putExtra(INCIDENCIA, incidencia);
                startActivity(intent);
                return true;
            case R.id.ver:
                incidencia = incidencias.get(itemSeleccionado);
                intent = new Intent(this,MainActivity.class);
                intent.putExtra(INCIDENCIA,incidencia);
                startActivity(intent);
                return true;
            case R.id.anadir_favorito:
                BaseDatos db = new BaseDatos(this);
                incidencia = incidencias.get(itemSeleccionado);
                db.insertarIncidencia(incidencia);
                break;
        }
        return super.onContextItemSelected(item);
    }
}
