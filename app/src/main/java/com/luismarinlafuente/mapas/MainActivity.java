package com.luismarinlafuente.mapas;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.luismarinlafuente.mapas.base.Incidencia;
import com.luismarinlafuente.mapas.util.Util;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;

import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.LATITUD;
import static com.luismarinlafuente.mapas.base.Constantes.LISTA_COMPLETA;
import static com.luismarinlafuente.mapas.base.Constantes.LONGITUD;
import static com.luismarinlafuente.mapas.base.Constantes.POINT;

/**
 * @author luismarinlafuente on 17/11/17
 */
public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener,
        MapboxMap.OnMapLongClickListener {

    Incidencia incidencia = null;
    //Incidencia inc = null;
    MapView mapView;
    MapboxMap mapbox;
    ArrayList<Incidencia> incidencias = null;
    double lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //generar la instancia de mapBox getResources().getString(R.string.mi_token) es lo mismo que
        //poner el token escrito en un string o con "comillas"
        Mapbox.getInstance(this, getResources().getString(R.string.mi_token));//esta linea siempre antes de setContenView
        setContentView(R.layout.activity_main);
        //lineas para pillar el mapView del layout crearlo
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.getMapAsync(this);

        Intent intent = getIntent();
        if (intent.getSerializableExtra(INCIDENCIA) != null)
            incidencia = (Incidencia) intent.getSerializableExtra(INCIDENCIA);
        if (intent.getSerializableExtra(LISTA_COMPLETA) != null)
            incidencias = (ArrayList<Incidencia>) intent.getSerializableExtra(LISTA_COMPLETA);
        mapView.onCreate(savedInstanceState);
        if(intent.getDoubleExtra(LATITUD,0)!=0&&intent.getDoubleExtra(LONGITUD,0)!=0){
            lat = intent.getDoubleExtra(LATITUD,0);
            lon = intent.getDoubleExtra(LONGITUD,0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_mapa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itDark:
                mapView.setStyleUrl("mapbox://styles/mapbox/dark-v9");
                return true;
            case R.id.itClaro:
                mapView.setStyleUrl("mapbox://styles/mapbox/streets-v9");
                return true;
            case R.id.itLight:
                mapView.setStyleUrl("mapbox://styles/mapbox/bright-v9");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        //MapView mapView = (MapView) findViewById(R.id.mapView);
        //poner un marcador en el mapa
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.setOnMapClickListener(new MapboxMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(@NonNull LatLng point) {
                    }
                });
            }
        });
        if (incidencia != null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(incidencia.getLatitud(), incidencia.getLongitud()))
                            .title(incidencia.getTitulo())
                            .snippet(incidencia.getDescipcion())
                    );
                }
            });
        }
        if (incidencias != null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    for (Incidencia inc : incidencias) {
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(inc.getLatitud(), inc.getLongitud()))
                                .title(inc.getTitulo())
                                .snippet(inc.getDescipcion())

                        );
                    }
                }
            });

        }
        if(lat !=0 && lon !=0){
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat,lon))
                    .title(getResources().getString(R.string.marcador))
                            .snippet(getResources().getString(R.string.usted_esta_aqui))
                    );
                }
            });
        }

        super.onResume();
    }


    /**
     * Called when the user clicks on the map view.
     *
     * @param point The projected map coordinate the user clicked on.
     */
    @Override
    public void onMapClick(@NonNull LatLng point) {

    }

    /**
     * Called when the user long clicks on the map view.
     *
     * @param point The projected map coordinate the user long clicked on.
     */
    @Override
    public void onMapLongClick(@NonNull LatLng point) {
        Intent intent = new Intent(this,AltaIncidencia.class);
        double [] latLong = new double[2];
        latLong[0]=point.getLatitude();
        latLong[1]=point.getLongitude();
        intent.putExtra(POINT,latLong);
        startActivity(intent);
    }



    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapbox = mapboxMap;
        addListener();
    }
    private void addListener(){
        mapbox.setOnMapClickListener(this);
        mapbox.setOnMapLongClickListener(this);
    }
}
