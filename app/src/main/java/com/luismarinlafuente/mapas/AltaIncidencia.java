package com.luismarinlafuente.mapas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.luismarinlafuente.mapas.base.BaseDatos;
import com.luismarinlafuente.mapas.base.Incidencia;
import com.luismarinlafuente.mapas.util.Util;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static com.luismarinlafuente.mapas.base.Constantes.ADD_INCIDENCIA_USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.MODIFICAR_INCIDENCIA_URL;
import static com.luismarinlafuente.mapas.base.Constantes.POINT;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;
import static com.luismarinlafuente.mapas.base.Constantes._NOTIFICACIONES;

public class AltaIncidencia extends AppCompatActivity implements View.OnClickListener{
    Incidencia incidencia = null;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta_incidencia);
        Button btAnadirIncidencia = (Button) findViewById(R.id.btAnadirIncidencia);
        btAnadirIncidencia.setOnClickListener(this);
        intent = getIntent();
        if (intent.getSerializableExtra(INCIDENCIA)!=null){
            btAnadirIncidencia.setText(getResources().getString(R.string.modificar));
            incidencia = (Incidencia) intent.getSerializableExtra(INCIDENCIA);
            EditText etTitulo = (EditText) findViewById(R.id.etTitulo);
            EditText etCalle = (EditText) findViewById(R.id.etCalle);
            EditText etMotivo = (EditText) findViewById(R.id.etMotivo);
            EditText etTramo = (EditText) findViewById(R.id.etTramo);
            EditText etObservaciones = (EditText) findViewById(R.id.etObservaciones);
            EditText etInicio = (EditText) findViewById(R.id.etInicio);
            EditText etFin = (EditText) findViewById(R.id.etFechaFin);
            EditText etDescripcion = (EditText) findViewById(R.id.etDescripcion);
            EditText etTipo = (EditText) findViewById(R.id.etTipo);
            etTitulo.setText(incidencia.getTitulo());
            etCalle.setText(incidencia.getCalle());
            etMotivo.setText(incidencia.getMotivo());
            etTramo.setText(incidencia.getTramo());
            etObservaciones.setText(incidencia.getObservaciones());
            etInicio.setText(Util.textoAFechaParaPintar(incidencia.getInicio()));
            etFin.setText(Util.textoAFechaParaPintar(incidencia.getFin()));
            etDescripcion.setText(incidencia.getDescipcion());
            etTipo.setText(incidencia.getTipo());
        }

    }

    @Override
    public void onClick(View view) {
        double[] lattLong = intent.getDoubleArrayExtra(POINT);
        InsertarModificar insertarModificar= null;
        switch (view.getId()){
            case R.id.btAnadirIncidencia:
                EditText etTitulo = (EditText) findViewById(R.id.etTitulo);
                EditText etCalle = (EditText) findViewById(R.id.etCalle);
                EditText etMotivo = (EditText) findViewById(R.id.etMotivo);
                EditText etTramo = (EditText) findViewById(R.id.etTramo);
                EditText etObservaciones = (EditText) findViewById(R.id.etObservaciones);
                EditText etInicio = (EditText) findViewById(R.id.etInicio);
                EditText etFin = (EditText) findViewById(R.id.etFechaFin);
                EditText etDescripcion = (EditText) findViewById(R.id.etDescripcion);
                EditText etTipo = (EditText) findViewById(R.id.etTipo);
                String titulo = etTitulo.getText().toString();
                String calle = etCalle.getText().toString();
                String motivo = etMotivo.getText().toString();
                String tramo = etTramo.getText().toString();
                String observaciones = etObservaciones.getText().toString();
                Date inicio = Util.textoAFechaParaPintar(etInicio.getText().toString());
                Date fin = Util.textoAFechaParaPintar(etFin.getText().toString());
                String descipcion = etDescripcion.getText().toString();
                String tipo = etTipo.getText().toString();

                if (incidencia == null){
                    double longitud = lattLong[1];
                    double latitud = lattLong[0];
                    incidencia = new Incidencia(0,titulo,calle,motivo,tramo,observaciones,inicio,fin,longitud,latitud,descipcion,tipo);
                    insertarModificar = new InsertarModificar(true,incidencia);
                    insertarModificar.execute();
                    if(_NOTIFICACIONES)
                        Toast.makeText(this,"Se ha añadido una Incidencia",Toast.LENGTH_LONG).show();
                }else{
                    incidencia.setTitulo(titulo);
                    incidencia.setCalle(calle);
                    incidencia.setMotivo(motivo);
                    incidencia.setTramo(tramo);
                    incidencia.setObservaciones(observaciones);
                    incidencia.setInicio(inicio);
                    incidencia.setFin(fin);
                    incidencia.setDescipcion(descipcion);
                    incidencia.setTipo(tipo);
                    if(incidencia.getIcono()==22){
                        BaseDatos db = new BaseDatos(this);
                        db.updateIncidencia(incidencia);
                    }else {
                        insertarModificar = new InsertarModificar(false,incidencia);
                        insertarModificar.execute();
                    }
                    if(_NOTIFICACIONES)
                        Toast.makeText(this,"Se ha modificado una Incidencia",Toast.LENGTH_LONG).show();

                }

                intent = new Intent(this, ListadoUbicaciones.class);
                startActivity(intent);


                break;
        }
    }
    private class InsertarModificar extends AsyncTask <Void, Void ,Void>{
        private boolean nuevo;
        Incidencia incidencia;

        public InsertarModificar(boolean nuevo, Incidencia incidencia) {
            this.nuevo = nuevo;
            this.incidencia = incidencia;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate1 = new RestTemplate();
            restTemplate1.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            if(nuevo){
                try {
                    restTemplate1.getForObject(URL_SPRING + ADD_INCIDENCIA_USUARIO + "?titulo=" + incidencia.getTitulo() +
                            "&calle=" + incidencia.getCalle() + "&motivo=" + incidencia.getMotivo() + "&tramo=" + incidencia.getTramo() +
                            "&observaciones=" + incidencia.getObservaciones() + "&inicio=" + Util.fechaAtextoInsert(incidencia.getInicio()) +
                            "&fin=" + Util.fechaAtextoInsert(incidencia.getFin()) + "&longitud=" + incidencia.getLongitud() +
                            "&latitud=" + incidencia.getLatitud() + "&descipcion=" + incidencia.getDescipcion() +
                            "&tipo=" + incidencia.getTipo(), Void.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                try {
                    restTemplate1.getForObject(URL_SPRING + MODIFICAR_INCIDENCIA_URL + "?id="+ incidencia.getId()+"&titulo=" + incidencia.getTitulo() +
                            "&calle=" + incidencia.getCalle() + "&motivo=" + incidencia.getMotivo() + "&tramo=" + incidencia.getTramo() +
                            "&observaciones=" + incidencia.getObservaciones() + "&inicio=" + Util.fechaAtextoInsert(incidencia.getInicio()) +
                            "&fin=" + Util.fechaAtextoInsert(incidencia.getFin()) + "&longitud=" + incidencia.getLongitud() +
                            "&latitud=" + incidencia.getLatitud() + "&descipcion=" + incidencia.getDescipcion() +
                            "&tipo=" + incidencia.getTipo(), Void.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
