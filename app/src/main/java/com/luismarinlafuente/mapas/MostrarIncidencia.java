package com.luismarinlafuente.mapas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.luismarinlafuente.mapas.base.BaseDatos;
import com.luismarinlafuente.mapas.base.Incidencia;
import com.luismarinlafuente.mapas.util.Util;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.luismarinlafuente.mapas.base.Constantes.ADD_INCIDENCIA_USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.ELIMINAR_INCIDENCIA_URL;
import static com.luismarinlafuente.mapas.base.Constantes.INCIDENCIA;
import static com.luismarinlafuente.mapas.base.Constantes.MODIFICAR_INCIDENCIA_URL;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;

public class MostrarIncidencia extends AppCompatActivity implements View.OnClickListener {
    Intent intent;
    Incidencia incidencia = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_incidencia);
        intent = getIntent();
        if(intent.getSerializableExtra(INCIDENCIA)!=null){
            incidencia = (Incidencia) intent.getSerializableExtra(INCIDENCIA);
            TextView tvTitulo = (TextView) findViewById(R.id.tvTitulo);
            TextView tvCalle = (TextView) findViewById(R.id.tvCalle);
            TextView tvMotivo = (TextView) findViewById(R.id.tvMotivo);
            TextView tvTramo = (TextView) findViewById(R.id.tvTramo);
            TextView tvObservaciones = (TextView) findViewById(R.id.tvObservacones);
            TextView tvInicio = (TextView) findViewById(R.id.tvInicio);
            TextView tvFin = (TextView) findViewById(R.id.tvFin);
            TextView tvDescripcion = (TextView) findViewById(R.id.tvDescripcion);
            TextView tvTipo = (TextView) findViewById(R.id.tvTipo);

            tvTitulo.setText(onPrepareString(getResources().getString(R.string.titulo_incidencia) , incidencia.getTitulo()));
            tvCalle.setText(onPrepareString(getResources().getString(R.string.calle) , incidencia.getCalle()));
            tvMotivo.setText(onPrepareString(getResources().getString(R.string.motivo) , incidencia.getMotivo()));
            tvTramo.setText(onPrepareString(getResources().getString(R.string.tramo), incidencia.getTramo()));
            tvObservaciones.setText(onPrepareString(getResources().getString(R.string.observaciones) , incidencia.getObservaciones()));
            tvInicio.setText(onPrepareString(getResources().getString(R.string.fecha_inicio) , Util.textoAFechaParaPintar(incidencia.getInicio())));
            tvFin.setText(onPrepareString(getResources().getString(R.string.fecha_fin), Util.textoAFechaParaPintar(incidencia.getFin())));
            tvDescripcion.setText(onPrepareString(getResources().getString(R.string.descripcion) , incidencia.getDescipcion()));
            tvTipo.setText(onPrepareString(getResources().getString(R.string.tipo) , incidencia.getTipo()));
        }
        Button btAtras = (Button) findViewById(R.id.btAtras);
        btAtras.setOnClickListener(this);
        Button btModificar = (Button) findViewById(R.id.btModificar);
        btModificar.setOnClickListener(this);
        Button btVerEnMapa = (Button) findViewById(R.id.btVerEnMapa);
        btVerEnMapa.setOnClickListener(this);
        Button btEliminar = (Button) findViewById(R.id.btEliminar);
        btEliminar.setOnClickListener(this);
        Button btComentar = (Button) findViewById(R.id.btComentar);
        btComentar.setOnClickListener(this);
        Button btComentarios = (Button) findViewById(R.id.btComentarios);
        btComentarios.setOnClickListener(this);
        if(incidencia.getTieneComentarios()==0){
            btComentarios.setEnabled(false);
        }
        if(incidencia.getIcono()==22){
            btComentar.setText(getResources().getString(R.string.eliminar_favorito));
            btEliminar.setEnabled(false);
        }
    }
    private String onPrepareString(String cabecera, String cuerpo){
        StringBuilder sb = new StringBuilder();
        return sb.append(cabecera).append(" ").append(cuerpo).toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btAtras:
                intent = new Intent(this, ListadoUbicaciones.class);
                startActivity(intent);
                break;
            case R.id.btModificar:
                intent = new Intent(this, AltaIncidencia.class);
                intent.putExtra(INCIDENCIA,incidencia);
                startActivity(intent);
                break;
            case R.id.btVerEnMapa:
                intent = new Intent(this,MainActivity.class);
                intent.putExtra(INCIDENCIA,incidencia);
                startActivity(intent);
                break;
            case R.id.btEliminar:
                    Eliminar eliminar = new Eliminar(incidencia);
                    eliminar.execute();
                    intent = new Intent(this, ListadoUbicaciones.class);
                    startActivity(intent);

                break;
            //todo hacer las activitis
            case  R.id.btComentar:
                if(incidencia.getIcono()==22) {
                    BaseDatos db = new BaseDatos(this);
                    db.eliminarIncidencia(incidencia.getId());
                    intent = new Intent(this,ListadoUbicaciones.class);
                    startActivity(intent);
                }else{
                    intent = new Intent(this,Comentar.class);
                    intent.putExtra(INCIDENCIA,incidencia);
                    startActivity(intent);
                }
                break;
            case R.id.btComentarios:
                intent = new Intent(this,Comentar.class);
                intent.putExtra(INCIDENCIA,incidencia);
                startActivity(intent);
                break;
        }
    }
    private class Eliminar extends AsyncTask<Void, Void ,Void> {
        private boolean nuevo;
        Incidencia incidencia;

        public Eliminar( Incidencia incidencia) {
            this.incidencia = incidencia;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate1 = new RestTemplate();
            restTemplate1.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate1.getForObject(URL_SPRING + ELIMINAR_INCIDENCIA_URL + "?id="+incidencia.getId() , Void.class);
            return null;
        }
    }
}
