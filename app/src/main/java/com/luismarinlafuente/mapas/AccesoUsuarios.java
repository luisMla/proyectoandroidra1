package com.luismarinlafuente.mapas;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.luismarinlafuente.mapas.base.Usuario;
import com.luismarinlafuente.mapas.util.Util;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import static com.luismarinlafuente.mapas.base.Constantes.ADD_INCIDENCIA_USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.ADD_USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.GET_USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.URL_SPRING;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO;
import static com.luismarinlafuente.mapas.base.Constantes.USUARIO_REGISTRADO;

public class AccesoUsuarios extends AppCompatActivity implements View.OnClickListener{
    private Usuario usuario = null;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceso_usuarios);
        Button btAcceder = (Button) findViewById(R.id.btAcceder);
        btAcceder.setOnClickListener(this);
        Button btRegistrarme = (Button) findViewById(R.id.btRegistrarme);
        btRegistrarme.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        AccesoRegistro accesoRegistro;
        switch (view.getId()){
            case R.id.btAcceder:
                EditText etLogg = (EditText) findViewById(R.id.etLoggin);
                EditText etPass = (EditText) findViewById(R.id.etPass);
                String loggin = etLogg.getText().toString();
                String pass = etPass.getText().toString();
                accesoRegistro = new AccesoRegistro(false,usuario,loggin,pass,this,intent);
                accesoRegistro.execute();


                break;
            case R.id.btRegistrarme:
                EditText etLogginReg = (EditText) findViewById(R.id.etLogginReg);
                EditText etPassReg = (EditText) findViewById(R.id.etPassReg);
                EditText etEmailReg = (EditText) findViewById(R.id.etEmailReg);
                usuario = new Usuario();
                usuario.setLoggin(etLogginReg.getText().toString());
                usuario.setEmail(etEmailReg.getText().toString());
                usuario.setPass(etPassReg.getText().toString());
                if(onCheckCredentials(usuario)){
                    accesoRegistro = new AccesoRegistro(true,usuario,this,intent);
                    accesoRegistro.execute();
                } else {
                    Toast.makeText(this, "Credenciales invalidas", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }
    private boolean onCheckCredentials(Usuario usuario){
        return isValidEmail(usuario.getEmail()) && isValidPassword(usuario.getPass()) && isValidPassword(usuario.getLoggin());
    }

    /**
     * Verificacion de email
     * @param email
     * @return
     */
    public boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * len del pass
     * @param pass
     * @return
     */
    public boolean isValidPassword(String pass){
        return pass.length() > 4;
    }

    private class AccesoRegistro extends AsyncTask<Void,Void,Void>{
        private boolean alta = false;
        private Usuario usuario;
        private String loggin;
        private String pass;
        private Context context;
        private Intent intent;

        public AccesoRegistro(boolean alta, Usuario usuario, String loggin, String pass, Context context, Intent intent) {
            this.alta = alta;
            this.usuario = usuario;
            this.loggin = loggin;
            this.pass = pass;
            this.context = context;
            this.intent = intent;
        }

        public AccesoRegistro(boolean alta, Usuario usuario, Context context, Intent intent) {
            this.alta = alta;
            this.usuario = usuario;
            this.context = context;
            this.intent = intent;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            if(!alta){
               try {
                   Usuario[] usuarios = restTemplate.getForObject(URL_SPRING + GET_USUARIO+ "?loggin=" +loggin + "&pass=" +pass ,Usuario[].class);
                   ArrayList<Usuario>usuarioArrayList = new ArrayList<>();
                   usuarioArrayList.addAll(Arrays.asList(usuarios));
                   if(usuarioArrayList.size() > 0){
                       USUARIO_REGISTRADO = usuarioArrayList.get(0);
                       intent = new Intent(context,ListadoUbicaciones.class);
                       startActivity(intent);
                   }else if (usuarioArrayList.size() == 0 ){
                        System.out.println("-----------------------error acceso");
                       intent = new Intent(context,ListadoUbicaciones.class);
                       startActivity(intent);
                   }
               }catch (Exception e ){
               }
            }
            if(alta){
                try {
                    restTemplate.getForObject(URL_SPRING + ADD_USUARIO + "?loggin=" +usuario.getLoggin()+"&pass=" +usuario.getPass()+
                            "&email=" + usuario.getEmail(),Void.class);
                    alta =false;
                    loggin = usuario.getLoggin();
                    pass = usuario.getPass();
                    doInBackground();

                }catch (Exception e){
                    intent = new Intent(context,ListadoUbicaciones.class);
                    startActivity(intent);
                }

            }
            return null;
        }
    }
}
